﻿using System;
using Models;
using DAL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using Models;
using DAL;


namespace WeChatDemo
{
    public partial class getLongMaterial1 : System.Web.UI.Page
    {
        //private MaterialModel db = new MaterialModel();
        //private HTTPRequest dal = new HTTPRequest();
        MaterialModel db = new MaterialModel();
        HTTPRequest dal = new HTTPRequest();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnMaterial_Click(object sender, EventArgs e)
        {
            getData();
            string token = new ACCESS_TOKEN().GetAccessToken();
            string jsonStr = "{{\"media_id\":\"{0}\" }}";
            jsonStr = string.Format(jsonStr, db.MediaId);
            JObject obj = JObject.Parse(jsonStr);
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/material/get_material?access_token={0}", token); //获取素材
            string resJson = dal.HttpPost(url, obj.ToString());
            this.ResultMaterial.InnerText = resJson;
        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            getData();
            string token = new ACCESS_TOKEN().GetAccessToken();
            string jsonStr = "{{\"type\":\"{0}\", \"offset\":{1},\"count\":{2} }}";
            jsonStr = string.Format(jsonStr, db.Type, db.Offset, db.Count);
            JObject obj = JObject.Parse(jsonStr); 
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token={0}", token);//获取素材列表
            string resJson = dal.HttpPost(url, obj.ToString());
            this.ResultMaterialList.InnerText = resJson;
        }
        public void getData()
        {
            db.MediaId = this.MediaId.Text;
            db.Type = this.TypeName.Value;
            db.Offset = Convert.ToInt32(this.Offset.Text);
            db.Count = Convert.ToInt32(this.Count.Text);
        }
    }
}