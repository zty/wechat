﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="addLongMaterial.aspx.cs" ValidateRequest="false" Inherits="WeChatDemo.addLongMaterial1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .ling{line-height:20px;margin-bottom:15px;
            top: 0px;
            left: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>新增图文素材 <small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start form for validation -->
                    <form id="demoForm" runat="server">
                        <div class="col-md-6 col-xs-12 ling">
                              <label for="fullname">标题 * :</label>
                              <%--<input type="text" id="fullname" class="form-control" name="fullname" required placeholder="请输入标题" />--%>
                              <asp:TextBox ID="TitleTxt" runat="server" CssClass="form-control" placeholder="请输入标题"></asp:TextBox>
                        </div>
             
                        <div class="col-md-6 col-xs-12 ling">
                              <label for="email">封面图片 * :</label>
                              <%--<input type="email" id="email" class="form-control" name="email" data-parsley-trigger="change" required placeholder="" />--%>
                              <asp:TextBox ID="MediaId" runat="server" CssClass="form-control" placeholder="图文消息的封面图片素材id（必须是永久 media_ID）"></asp:TextBox>
                        </div>
                        <div class="col-md-6 col-xs-12 ling">
                              <label for="email">作者 * :</label>
                              <%--<input type="email" id="email" class="form-control" name="email" data-parsley-trigger="change" required placeholder="" />--%>
                              <asp:TextBox ID="Author" runat="server" CssClass="form-control" placeholder="请输入作者"></asp:TextBox>
                         </div>
                         <div class="col-md-6 col-xs-12 ling">
                              <label for="email">摘要 * :</label>
                              <%--<input type="email" id="email" class="form-control" name="email" data-parsley-trigger="change" required placeholder="" />--%>
                              <asp:TextBox ID="Digest" runat="server" CssClass="form-control" placeholder="图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空"></asp:TextBox>
                         </div>
                         <div class="col-md-12 col-xs-12 ling">
                               <label for="email">原文地址 * :</label>
                               <%--<input type="email" id="email" class="form-control" name="email" data-parsley-trigger="change" required placeholder="" />--%>
                               <asp:TextBox ID="ContentSourceUrl" runat="server" CssClass="form-control" placeholder="图文消息的原文地址，即点击“阅读原文”后的URL"></asp:TextBox>
                         </div>
                        <div class="col-md-4 col-xs-12 ling">
                             <label>是否显示封面 *:</label>
                              <p style="margin-left:17%">
                                是:
                                <input type="radio" class="flat" name="Show" id="ShowM" value="1" required/> 
                                否:
                                <input type="radio" class="flat" name="Show" id="ShowF" value="0" checked="" />
                              </p>
                              <div class="hidden">
                                    <asp:RadioButton ID="ShowTure" runat="server" Text="1" GroupName="Show"/>
                                    <asp:RadioButton ID="ShowFalse" runat="server" Text="0" GroupName="Show" checked />
                               </div>
                        </div>
                        <div class="col-md-4 col-xs-12 ling">
                             <label>是否打开评论 *:</label>
                              <p style="margin-left:17%">
                                是:
                                <input type="radio" class="flat" name="Open" id="OpenM" value="1" checked="" required/>
                                否:
                                <input type="radio" class="flat" name="Open" id="OpenF" value="0"/>
                              </p>
                              <div class="hidden">
                                    <asp:RadioButton ID="OpenTure" runat="server" Text="1" GroupName="Open" checked />
                                    <asp:RadioButton ID="OpenFalse" runat="server" Text="0" GroupName="Open" />
                               </div>
                        </div>
                        <div class="col-md-4 col-xs-12 ling">
                             <label>是否粉丝才可评论 *:</label>
                              <p style="margin-left:17%">
                                是:
                                <input type="radio" class="flat" name="Fans" id="FansM" value="1" required/> 
                                否:
                                <input type="radio" class="flat" name="Fans" id="FansF" value="0"/ checked="" >
                              </p>
                              <div class="hidden">
                                    <asp:RadioButton ID="FansTure" runat="server" Text="1" GroupName="Fans" />
                                    <asp:RadioButton ID="FansFalse" runat="server" Text="0" GroupName="Fans" checked />
                               </div>
                        </div>
                        <div class="col-md-12 col-xs-12 ling">
                               <label for="email">内容 * :</label>
<%--                               <textarea id="" required="required" rows="17" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                                     data-parsley-validation-threshold="10" ></textarea>--%>
                            <textarea id="Content" runat="server" rows="17" class="form-control" placeholder="图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS"></textarea>
                        </div>

                          <br/>
                        <div class="col-md-12">
                            <%--<asp:Button ID="Button1" runat="server" Text="提交" CssClass="btn btn-primary" OnClick="Button1_Click" />--%>
                            <div class="input-group">
                            <span class="input-group-btn">
                                              <asp:Button ID="Button1" runat="server" Text="提交" CssClass="btn btn-primary" OnClick="Button1_Click" />
                                          </span>
                                 <asp:TextBox ID="ResultTxt" runat="server" CssClass="form-control" placeholder="接口返回结果"></asp:TextBox>
                          </div>
                        </div>
                        
                    </form>
                    <!-- end form for validations -->

                  </div>
                </div>


              </div>
      </div>
    <script>
        function alert() {
            alert("成功");
        }
        function IsChecked(num) {
            switch (num) {
                case 0:
                    $('#ContentPlaceHolder1_ShowTure').prop('checked', true);
                    break;
                case 1:
                    $('#ContentPlaceHolder1_ShowFalse').prop('checked', true);
                    break;
                case 2:
                    $('#ContentPlaceHolder1_OpenTure').prop('checked', true);
                    break;
                case 3:
                    $('#ContentPlaceHolder1_OpenFalse').prop('checked', true);
                    break;
                case 4:
                    $('#ContentPlaceHolder1_FansTure').prop('checked', true);
                    break;
                case 5:
                    $('#ContentPlaceHolder1_FansFalse').prop('checked', true);
                    break;
            }
        }
    </script>
</asp:Content>