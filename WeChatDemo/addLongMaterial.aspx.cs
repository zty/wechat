﻿using System;
using Models;
using DAL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;

namespace WeChatDemo
{
    public partial class addLongMaterial1 : System.Web.UI.Page
    {
        private MaterialModel db = new MaterialModel();
        private HTTPRequest dal = new HTTPRequest();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            getData();
            string token = new ACCESS_TOKEN().GetAccessToken();
            var jsonObj = new { articles = new List<object>() { new { title = db.Title, thumb_media_id = db.ThumbMediaId, author = db.Author, digest = db.Digest, show_cover_pic = db.ShowCoverPic, content = db.Content, content_source_url = db.Url, need_open_comment = db.NeedOpenComment, only_fans_can_comment = db.OnlyFansCanComment } } };
            //string jsonStr = "{{\"articles\":[{{\"title\":\"{0}\",\"thumb_media_id\":\"{1}\",\"author\":\"{2}\",\"digest\":\"{3}\",\"show_cover_pic\":{4},\"content\":\"{5}\",\"content_source_url\":\"{6}\",\"need_open_comment\":{7},\"only_fans_can_comment\":{8}}}]}}";
            //jsonStr = string.Format(jsonStr, db.Title, db.ThumbMediaId, db.Author, db.Digest, db.ShowCoverPic, db.Content, db.Url, db.NeedOpenComment, db.OnlyFansCanComment);
            var jsonStr = jsonObj.ToJson();
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/material/add_news?access_token={0}", token);
            string resJson = dal.HttpPost(url, jsonStr);
            this.ResultTxt.Text = resJson;
        }
        public void getData()
        {
            db.Title = this.TitleTxt.Text;
            db.ThumbMediaId = this.MediaId.Text;
            db.Author = this.Author.Text;
            db.Digest = this.Digest.Text;
            db.Url = this.ContentSourceUrl.Text;
            db.Content = this.Content.InnerText;

            if (ShowTure.Checked)
                db.ShowCoverPic = Convert.ToInt32(this.ShowTure.Text);
            if (ShowFalse.Checked)
                db.ShowCoverPic = Convert.ToInt32(this.ShowFalse.Text);
            if (OpenTure.Checked)
                db.NeedOpenComment = Convert.ToInt32(this.OpenTure.Text);
            if (OpenFalse.Checked)
                db.NeedOpenComment = Convert.ToInt32(this.OpenFalse.Text);
            if (FansTure.Checked)
                db.OnlyFansCanComment = Convert.ToInt32(this.FansTure.Text);
            if (FansFalse.Checked)
                db.OnlyFansCanComment = Convert.ToInt32(this.FansFalse.Text);
        }
    }
}