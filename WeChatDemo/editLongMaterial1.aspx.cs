﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace WeChatDemo
{
    public partial class editLongMaterial : System.Web.UI.Page
    {
        //private string html = "<p style='font-size: 16px;white-space: normal;max-width: 100%;min-height: 1em;color: rgb(62, 62, 62);background-color: rgb(255, 255, 255);line-height: 25.6px;box-sizing: border-box !important;word-wrap: break-word !important;'><span style='max-width: 100%;line-height: 16px;font-family: 宋体;font-size: 14px;background-color: rgb(253, 234, 218);box-sizing: border-box !important;word-wrap: break-word !important;'><strong style='max-width: 100%;color: rgb(102, 102, 102);box-sizing: border-box !important;word-wrap: break-word !important;'><span style='padding: 4px 10px;max-width: 100%;color: rgb(0, 0, 0);box-shadow: rgb(165, 165, 165) 4px 4px 2px;box-sizing: border-box !important;word-wrap: break-word !important;'>导语</span></strong><br style='max-width: 100%;box-sizing: border-box !important;word-wrap: break-word !important;'></span></p><blockquote style='padding-top: 10px;padding-right: 10px;padding-bottom: 10px;border-top: 3px solid rgb(201, 201, 201);border-right: 3px solid rgb(201, 201, 201);border-bottom: 3px solid rgb(201, 201, 201);border-left-color: rgb(201, 201, 201);font-size: 16px;white-space: normal;max-width: 100%;color: rgb(62, 62, 62);background-color: rgb(255, 255, 255);line-height: 25.6px;box-shadow: rgb(170, 170, 170) 0px 0px 10px;box-sizing: border-box !important;word-wrap: break-word !important;'><p style='max-width: 100%;min-height: 1em;font-family: 微软雅黑;line-height: 25.6px;box-sizing: border-box !important;word-wrap: break-word !important;'><span style='background-color: rgb(253, 234, 218);color: rgb(38, 38, 38);font-family: 宋体;font-size: 14px;'>盛安德郑州团队近期在跟进一个新的澳洲机会，客户Matthew</span><span style='background-color: rgb(253, 234, 218);color: rgb(38, 38, 38);font-family: 宋体;font-size: 14px;'>来到中国，和团队针对项目启动前的需求确认和产品规划进行了更进一步的沟通。在近两周的时间里，客户和团队碰撞出了很多火花，同时也引发了团队的很多思考。从文中提到的几点开会过程中有意思的事情，我们能明确感受到：不管是客户还是团队，都在沟通的过程中不断修正自己的想法和思路，双方在齐心协力的为了同一个目标而努力，协同工作；客户和团队是合作伙伴，而不是简单的甲方乙方……</span></p></blockquote><br><p> Matthew是个澳洲客户，前期有过很长时间的沟通和推进，我们对业务和项目需求目标大概也了解。但是针对第一个要发布的版本，要做成具体什么样的产品还是两眼一抹黑。故此，客户来我们办公室两周，专门讨论具体细节。期望经过两周的密集讨论，我们能有若干产出，比如：</p><ul class='list-paddingleft-2' style='list-style-type: circle;'><li><p>所有的User Story</p></li><li><p>业务流程图清晰</p></li><li><p>数据库结构定义完毕</p></li><li><p>系统架构和特定功能点的技术方案</p></li><li><p>Wireframe定义完毕</p></li><li><p>核心页面的最终效果图</p></li><li><p>开发计划精确到每个Sprint的范围</p></li></ul><p style = 'text-align:center;box-sizing: border-box;margin-bottom: 15px;' ><img src= 'http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5SQA5c9xPY66sb0qYHCy34WT4VJE8OkhDaOBm1TVE7xdFJLxrDm8I7fRxpaG8PX1FAusL7SHwTibDjw/0?wx_fmt =jpeg'></ p ><br><p> 测试公众号.....</p><p> 测试文章.....</ p ><br><br><img src= 'http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5SQA5c9xPY66sb0qYHCy34WTXJZPib5jyubJxyvgqkF67PNIl0A5V2eWtJ0U3RzZ9pub7iaZq3dRv6fQ/0?wx_fmt =jpeg' /> ";
        protected void Page_Load(object sender, EventArgs e)
        {
            #region [ html ]
            string html = "<p>　　北京盛安德科技发展有限公司（以下简称“盛安德”）成立于2001年，是国际领先的软件开发服务提供商，专注于为客户开发“快速解决问题、创造真正价值“的软件。</p><p>　　我们的愿景和目标是“成为最适合程序员工作和发展的公司”。成立17年以来，我们一直致力于企业管理理念的创新，以提供自由宽松、开放合作的环境让程序员有机会去成长和改变。</p><p>　　“自由创新、开放合作”的企业文化由此培养了越来越多的程序员成长为独立自主，有创造力的敏捷开发工程师。他们能够站在客户的立场上，立足于客户的业务模式和信息化目标，与客户共同面对问题，协同创新，开发出能快速解决客户问题，给客户带来真正价值的软件。这也是盛安德的核心优势, 在全球1000+的客户中赢得了良好的口碑。</p><p>　　盛安德秉承“为客户创造真正价值”的服务宗旨，根据客户（企业）要解决的问题以及差异化的业务模型，针对不同客户量身提供由1~5个敏捷开发工程师组成的<a style='color: #d21919'>敏捷小团队</a>服务，帮助中小企业以最小的信息化投入，获得最为直接的收益。而在这个过程中，程序员在帮助客户解决问题、创造价值的同时，自身的价值也在不断提升，盛安德也因此离自己的愿景和目标越来越近。</p><div style='margin-top: 10px; margin-bottom: 40px;'><div style='width: 160px;height: 160px;border-radius: 100%;margin: 0 auto;margin-top: 50px;border: 1px solid #dedede;overflow: hidden;'><img src='http://mmbiz.qpic.cn/mmbiz_png/YzRib8XQo5STKlhhXXP7fDL1QSFZcG5aoWBYft52Hu5iaGbJLbmtLZfZVy8QuYM59U4TCuL8pEQ1EdekTkjZfpow/0?wx_fmt=png' style='max-width: 100%;'></div><p style='font-size: 24px; text-align: center;'>A message from Jerry Zhang</p><p style='font-size: 18px; text-align: center; margin-bottom: 30px;'><span>——盛安德价值观探讨</span></p><p>　　近期和各分公司负责人沟通，大家对我们的使命、愿景提出一些质疑。很高兴谈到这些话题，促使我们对问题做更加深入的思考。</p><p>　　问题是这样的：如果我们的使命是提升程序员，对于公司管理层可以成为一个比较明确的工作方向，但对于占公司绝大多数的程序员，他们该做些什么？公司价值观也把程序员摆在第一位，那程序员应该关注什么？</p><p>　　我想先谈谈使命、愿景和价值观三个概念，确保大家理解一致。一个公司的使命，就是公司存在的意义。公司的使命要回答的问题，是为什么要建立这家公司，我们的努力是否仅仅为了生存？愿景是为实现使命制定的小目标，阶段性目标。如果完成使命要数十年努力，愿景就是几年、十几年内的小目标。价值观是为实现愿景和使命，大家一致认同的理念或若干行为准则，也可以看作是达成愿景和使命的方法与手段。</p><p>　　失去使命的公司很容易成为一部赚钱机器。我有一位企业负责人朋友，有一天向我诉苦，每天忙着为团队找项目做，项目结束又为生存去找新的项目，这么多年下来感到很累，不知道自己为什么坚持。我也有过一样的困惑，创建公司最初是为了自己能多赚点钱。直到我发现让大家都能发展，都能赚到钱才是企业可持续发展的核心动力。所以我们曾大幅提高员工工资，鼓励和发展分公司，建立海外分支结构，鼓励外派程序员出国工作，尝试各种不同模式（合伙人和交付中心），以实现更多人在推动企业发展的同时，受惠于发展。盛安德的发展过程就是员工发展的过程。我们把它总结成自己的使命，就是提升和体现程序员价值。</p><p>　　为了让我们的使命能够更加具体和有效，我在目前的使命基础上，增加了一句话：提升和体现程序员价值，推动中国软件业变革。要改变世界，只有改变自己。只有程序员不断改进，软件业变革才可能发生，软件才能更好地为用户创造价值。</p><p>　　我们培养和提升程序员，改变的不仅仅是技术能力，更重要的是对软件的认知，这些改变最终会影响整个软件业，我希望这也能成为每一位盛安德程序员努力的方向。</p><p>　　作为阶段性的小目标，我们的愿景是要建立一家最适合软件工程师工作的公司。这与一家传统外包公司的形象似乎相差太远，如果一定要把我们归类到外包公司，那我们就需要颠覆外包业务在国人心目中的印象。要怎样做？</p><p>　　建立价值观，以程序员的发展作为企业发展的基石，就是程序员第一。任何一家企业的发展战略最终都是员工发展战略的延伸。通过培训并提供机会，让员工最大限度提升和发展自己的能力。这些能力只能在为客户创造价值的过程中积累并体现出来。通过单纯的学习能获得的是知识或技能，知识与技能是解决问题和创造价值的工具，利用工具去解决问题才是创造价值的能力。</p><p>　　敏捷宣言本身就是一套价值观，除了程序员第一之外，敏捷宣言也是盛安德价值观重要的组成部分。 敏捷宣言在这里不具体解释了，但希望大家都能记住这四句核心价值观：</p><p><ul style='padding-left: 85px;line-height: 35px;list-style: circle; letter-spacing: 2px;'><li><b style='color: #d21919;'>个体和互动</b>高于流程和工具</li><li><b style='color: #d21919;'>工作的软件</b>高于详尽的文档</li><li><b style='color: #d21919;'>客户合作</b>高于合同谈判</li><li><b style='color: #d21919;'>响应变化</b>高于遵循计划</li></ul></p><p>　　这四句话在我们工作中随时可以应用，不仅在软件开发中，公司日常管理和运营工作都同样有效。但它之所以被提出来，是因为它与我们习以为常的工作模式完全相悖。工具、文档、合同和计划的背后都是明确责权与分工，基于明确分工进行的合作是以工作内容为导向的，只有负责人（经理）对要解决的问题负责。而敏捷价值观是以（解决）问题为导向，对团队中每一位成员的要求和依赖度都更高，也更容易体现个人价值。敏捷价值观是西方文明结合古老东方智慧的先进理念，是以人为核心的发展观和价值观。这与我们想表达的价值观是重合的。</p><p style='font-weight: bold; margin-top: 30px;'>张纪伟</p><p>CEO, Shinetech Software Inc.</p></div><div style='background-color: #f1f1f1; padding:15px 0;'><h3 style='font-size: 17px; padding: 0 10px; '> 盛安德拥有</h3><div style='padding: 0 15px;'><div style='font-size:2em;margin-right:0;'><div style='line-height: 30px;'><p><span style='color: #ff0066'>1000<sup>+</sup></span><span style='font-size:16px;'>建立合作的客户</span></p></div><div style='line-height: 30px;'><p><span style='color: #00a2e6'>50<sup>+</sup></span><span style='font-size:16px;'>正在合作超过两年的客户</span></p></div><div style='line-height: 30px;'><p><span style='color: #548beb'>30<sup>+</sup></span><span style='font-size:16px;'>客户分布的国家，包括中国、美国、英国、澳大利亚、瑞士、德国、芬兰、荷兰、比利时等</span></p></div><div style='line-height: 30px;'><p><span style='color: #5eae00'>400<sup>+</sup></span><span style='font-size:16px;'>员工在全球21个办公地点为客户提供服务</span></p></div><div style='line-height: 30px;'><p><span style='color: #2db5be'>40<sup>+</sup></span><span style='font-size:16px;'>员工从一线工程师成长为负责交付和管理的角色</span></p></div><div style='line-height: 30px;'><p><span style='color: #ff6600'>0</span><span style='font-size:16px;'>从未和客户产生过知识产权相关的纠纷</span></p></div></div><div style='clear: both'></div></div><h3 style='font-size: 17px;padding: 0 10px;'>价值观和传统</h3><ul style='padding: 0 15px 0 42px;list-style-type: circle;font-size: 15px;'><li style='margin: 25px 0;'><strong style='color: #d21919;'>诚实，不说谎</strong> – 这是每一个盛安德员工坚守的第一准则。</li><li style='margin: 25px 0;'><strong style='color: #d21919;'>不做自有知识产权的软件产品</strong> – 做纯粹的技术服务公司, 从根本上保护客户的知识产权。</li><li style='margin: 25px 0;'><strong style='color: #d21919;'>坚持公平的商业规则</strong> – 我们一直坚持不提供回扣等有违商业公平原则的行为, 并且时刻保持审视自身，给客户创造真正的价值。</li><li style='margin: 25px 0;'><strong style='color: #d21919;'>敏捷</strong> – 从2006年我们开始在整个公司倡导并实践敏捷理念，借助敏捷拉近工程师和市场的距离。</li><li style='margin: 25px 0;'><strong style='color: #d21919;'>精简运营团队规模</strong> – 只有这样才能把最好的资源提供给一线员工。</li><li style='margin: 25px 0;'><strong style='color: #d21919;'>投资于员工</strong>– 为员工提供多样化的培训和发展平台。</li></ul><p style='padding: 0 15px;'>这些价值观和传统始终指导着我们的业务、机制、品牌、员工发展等每一个业务环节。</p><p style='text-align:center;box-sizing: border-box;margin-bottom: 15px;'><img src='http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5STKlhhXXP7fDL1QSFZcG5aoApcK4lRR69RqOITQcMiaFn8iaj6IWa6ibgfO4Z7oIvHlvo7picqnV3XuNQ/0?wx_fmt=jpeg' /></p></div>";
            #endregion
            //html = Server.UrlEncode(html);
            //html = Server.HtmlEncode(html);
            string token = new ACCESS_TOKEN().GetAccessToken();
            Response.Write(token);
            Response.Write("<br>");

            string jsonStr = "{{\"media_id\":\"{0}\",\"index\":{1},\"articles\":{{\"title\":\"{2}\",\"thumb_media_id\":\"{3}\",\"author\":\"{4}\",\"digest\":\"{5}\",\"show_cover_pic\":{6},\"content\":\"{7}\",\"content_source_url\":\"{8}\",\"need_open_comment\":{9},\"only_fans_can_comment\":{10}}}}}";
            jsonStr = string.Format(jsonStr, "K0FgA3ctFn92CDFw_ks93MEEPtg7BYDkhrLp8Jfnf0k", 0, "我们是谁，我们做什么？", "K0FgA3ctFn92CDFw_ks93LFn_b87_IEXJRqVUCRwtmg", "Albert","", 0, html, "http://www.shinetechchina.com.cn/about_us.shtml", 1,0);
            JObject obj = JObject.Parse(jsonStr);
            //var obj = new JavaScriptSerializer().Serialize(jsonStr);
            //obj = HttpContext.Current.Server.UrlDecode(obj);
            //obj = HttpContext.Current.Server.HtmlDecode(obj);

            //Response.Write(obj);

            string url = string.Format("https://api.weixin.qq.com/cgi-bin/material/update_news?access_token={0}", token);
            HttpImgTxt(url, obj.ToString());
        }

        /// <summary>
        /// 上传图文素材
        /// </summary>
        /// <returns></returns>
        public string HttpImgTxt(string posturl, string postData)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            byte[] data = encoding.GetBytes(postData);
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(posturl) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                Response.Write(content);
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }
    }
}