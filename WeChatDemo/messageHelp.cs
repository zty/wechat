﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Text;
using System.Web.Security;
using System.Xml;

namespace WeChatDemo
{
    /// <summary>
    /// 接受/发送消息帮助类
    /// </summary>
    public class messageHelp
    {
        //返回消息
        public string ReturnMessage(string postStr)
        {
            string responseContent = "";
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(new System.IO.MemoryStream(System.Text.Encoding.GetEncoding("GB2312").GetBytes(postStr)));
            XmlNode MsgType = xmldoc.SelectSingleNode("/xml/MsgType");
            if (MsgType!=null)
            {
                switch (MsgType.InnerText)
                {
                    case "event":
                        responseContent=EventHandle(xmldoc);//事件处理
                        break;
                    case "text":
                        responseContent=TextHandle(xmldoc);//接受文本消息处理
                        break;
                    default:
                        break;
                }
            }
            return responseContent;
        }
        //事件
        public string EventHandle(XmlDocument xmldoc)
        {
            string responseContent = "";
            XmlNode Event = xmldoc.SelectSingleNode("/xml/Event");
            XmlNode EventKey = xmldoc.SelectSingleNode("/xml/EventKey");
            XmlNode ToUserName = xmldoc.SelectSingleNode("/xml/ToUserName");
            XmlNode FromUserName = xmldoc.SelectSingleNode("/xml/FromUserName");
            if (Event!=null)
            {
                //菜单单击事件
                if (Event.InnerText.Equals("CLICK"))
                {
                    if (EventKey.InnerText.Equals("click_News"))//click_one
                    {
                        responseContent = string.Format(ReplyType.Message_News_Main,
                            FromUserName.InnerText,
                            ToUserName.InnerText,
                            DateTime.Now.Ticks,
                              "5",
                             string.Format(ReplyType.Message_News_Item, "17年全球软件开发服务经验", "",
                             "http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5STKlhhXXP7fDL1QSFZcG5aoBbicibQREyWnBUnogbuAT3xgGZxwROhxzneqxAaicYibpqiao6RqLcibTWCg/0?wx_fmt=jpeg",
                             "https://w.url.cn/s/AjxQtj1") +
                             string.Format(ReplyType.Message_News_Item, "我们是谁，我们做什么", "",
                             "http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5STKlhhXXP7fDL1QSFZcG5aoBbicibQREyWnBUnogbuAT3xgGZxwROhxzneqxAaicYibpqiao6RqLcibTWCg/0?wx_fmt=jpeg",
                             "https://w.url.cn/s/AhwOE7r") +
                             string.Format(ReplyType.Message_News_Item, "我们充满活力的文化", "",
                             "https://w.url.cn/s/AyjLHi0",
                             "http://www.soso.com/") +
                             string.Format(ReplyType.Message_News_Item, "成长足迹", "",
                             "https://w.url.cn/s/AyjLHi0",
                             "http://www.soso.com/") +
                             string.Format(ReplyType.Message_News_Item, "奖项", "",
                             "https://w.url.cn/s/AyjLHi0",
                             "http://www.soso.com/"));
                    }
                    else if (EventKey.InnerText.Equals("click_Join"))//click_two
                    {
                        responseContent = string.Format(ReplyType.Message_News_Main,
                            FromUserName.InnerText,
                            ToUserName.InnerText,
                            DateTime.Now.Ticks,
                            "5",
                             string.Format(ReplyType.Message_News_Item, "我们是谁，我们做什么", "",
                             "http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5STKlhhXXP7fDL1QSFZcG5aoBbicibQREyWnBUnogbuAT3xgGZxwROhxzneqxAaicYibpqiao6RqLcibTWCg/0?wx_fmt=jpeg",
                             " https://w.url.cn/s/AhwOE7r") +
                             string.Format(ReplyType.Message_News_Item, "招聘 .NET工程师", "",
                             "http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5STKlhhXXP7fDL1QSFZcG5aoBbicibQREyWnBUnogbuAT3xgGZxwROhxzneqxAaicYibpqiao6RqLcibTWCg/0?wx_fmt=jpeg",
                             "http://www.soso.com/") +
                             string.Format(ReplyType.Message_News_Item, "招聘 JAVA工程师", "",
                             "https://w.url.cn/s/AyjLHi0",
                             "http://www.soso.com/") +
                             string.Format(ReplyType.Message_News_Item, "招聘 WEB前端工程师", "",
                             "https://w.url.cn/s/AyjLHi0",
                             "http://www.soso.com/") +
                             string.Format(ReplyType.Message_News_Item, "招聘 移动开发", "",
                             "https://w.url.cn/s/AyjLHi0",
                             "http://www.soso.com/"));
                    }
                    else if (EventKey.InnerText.Equals("click_Culture"))//click_two
                    {
                        responseContent = string.Format(ReplyType.Message_News_Main,
                            FromUserName.InnerText,
                            ToUserName.InnerText,
                            DateTime.Now.Ticks,
                            "3",
                             string.Format(ReplyType.Message_News_Item, "敏捷开发", "",
                             " http://ds7m2w.natappfree.cc/images/jianjie.jpg",
                             " https://mp.weixin.qq.com/s?__biz=MzIxMzQ1MTcyMw==&mid=100000002&idx=1&sn=cf8b68012b150fc2ebcafd15b5eb9d9c&chksm=17b7d7a620c05eb072e9241825035887fae64072f2399d0c3e9c89f08b8c864c752830fb7df7#rd") +
                             string.Format(ReplyType.Message_News_Item, "与客户面对面确认需求是什么样的体验？", "",
                             " http://ds7m2w.natappfree.cc/images/jianjie.jpg",
                             "https://mp.weixin.qq.com/s?__biz=MzIxMzQ1MTcyMw==&mid=100000010&idx=1&sn=f62703219b15e685ae6a8ad42520160e&chksm=17b7d7ae20c05eb89cf5839911b75100c86d197633179144d27242d1ba81b18a66c9ffc06201#rd") +
                             string.Format(ReplyType.Message_News_Item, "如何开发出“快速解决问题、创造真正价值”的软件？", "",
                             " http://ds7m2w.natappfree.cc/images/jianjie.jpg",
                             "http://www.soso.com/"));
                    }
                    else if (EventKey.InnerText.Equals("click_Case"))//click_two
                    {
                        responseContent = string.Format(ReplyType.Message_News_Main, 
                            FromUserName.InnerText, 
                            ToUserName.InnerText, 
                            DateTime.Now.Ticks, 
                            "4",
                             string.Format(ReplyType.Message_News_Item,"经典案例","",
                             " http://ds7m2w.natappfree.cc/images/jianjie.jpg",
                             " http://ds7m2w.natappfree.cc/") +
                             string.Format(ReplyType.Message_News_Item, "旅游订单管理系统", "",
                             " http://ds7m2w.natappfree.cc/images/jianjie.jpg",
                             "http://www.soso.com/") +
                             string.Format(ReplyType.Message_News_Item, "自助仓储管理系统", "",
                             " http://ds7m2w.natappfree.cc/images/jianjie.jpg",
                             "http://www.soso.com/") +
                             string.Format(ReplyType.Message_News_Item, "教育评测系统", "",
                             " http://ds7m2w.natappfree.cc/images/jianjie.jpg",
                             "http://www.soso.com/"));
                    }
                    else if (EventKey.InnerText.Equals("click_Contact"))//click_three
                    {
                        responseContent = string.Format(ReplyType.Message_Text,
                            FromUserName.InnerText,
                            ToUserName.InnerText,
                            DateTime.Now.Ticks,
                            "北京盛安德科技发展有限公司 \r\nE-Mail：zhangtianyi@shinetechchina.com \r\n电话：18133513997 \r\n地址：秦皇岛市经济技术开发区洋河道12号楼E谷创想空间B区3层");
                    }
                }
            }
            return responseContent;
        }
        //接受文本消息
        public string TextHandle(XmlDocument xmldoc)
        {
            string responseContent = "";
            XmlNode ToUserName = xmldoc.SelectSingleNode("/xml/ToUserName");
            XmlNode FromUserName = xmldoc.SelectSingleNode("/xml/FromUserName");
            XmlNode Content = xmldoc.SelectSingleNode("/xml/Content");
            if (Content != null)
            {
                responseContent = string.Format(ReplyType.Message_Text, 
                    FromUserName.InnerText, 
                    ToUserName.InnerText, 
                    DateTime.Now.Ticks, 
                    "欢迎使用微信公共账号，您输入的内容为：" + Content.InnerText+"\r\n<a href=\"http://www.baidu.com\">点击进入</a>");
            }
            return responseContent;
        }

        //写入日志
        public void WriteLog(string text)
        {
            StreamWriter sw = new StreamWriter(HttpContext.Current.Server.MapPath(".") + "\\log.txt", true);
            sw.WriteLine(text);
            sw.Close();//写入
        }
    }

    //回复类型
    public class ReplyType
    {
        /// <summary>
        /// 普通文本消息
        /// </summary>
        public static string Message_Text
        {
            get { return @"<xml>
                            <ToUserName><![CDATA[{0}]]></ToUserName>
                            <FromUserName><![CDATA[{1}]]></FromUserName>
                            <CreateTime>{2}</CreateTime>
                            <MsgType><![CDATA[text]]></MsgType>
                            <Content><![CDATA[{3}]]></Content>
                            </xml>"; }
        }
        /// <summary>
        /// 图文消息主体
        /// </summary>
        public static string Message_News_Main
        {
            get
            {
                return @"<xml>
                            <ToUserName><![CDATA[{0}]]></ToUserName>
                            <FromUserName><![CDATA[{1}]]></FromUserName>
                            <CreateTime>{2}</CreateTime>
                            <MsgType><![CDATA[news]]></MsgType>
                            <ArticleCount>{3}</ArticleCount>
                            <Articles>
                            {4}
                            </Articles>
                            </xml> ";
            }
        }
        /// <summary>
        /// 图文消息项
        /// </summary>
        public static string Message_News_Item
        {
            get
            {
                return @"<item>
                            <Title><![CDATA[{0}]]></Title> 
                            <Description><![CDATA[{1}]]></Description>
                            <PicUrl><![CDATA[{2}]]></PicUrl>
                            <Url><![CDATA[{3}]]></Url>
                            </item>";
            }
        }
    }
}