﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;


namespace WeChatDemo
{
    public class ACCESS_TOKEN
    {
        public string GetAccessToken()
        {
            if (File.Exists(HttpContext.Current.Server.MapPath(".\\token.txt")))
            {
                //存在文件
                string[] tokenArray = ReaderToken().Split(',');
                //获取当前时间
                DateTime nowTime = DateTime.Now;
                //获取token时间
                DateTime tokenTime = Convert.ToDateTime(tokenArray[1]);
                //计算相差时间(秒)
                TimeSpan differ = nowTime.Subtract(tokenTime).Duration();
                int num = Convert.ToInt32(differ.TotalSeconds);
                if (num > 7100)
                {
                    string token = this.GetToken();
                    return token;
                }
                else
                {
                    return tokenArray[0];
                }
            }
            else
            {
                //不存在文件
                string token = this.GetToken();
                return token;
            }
            
        }

        //public string GetPage(string appid, string secret)
        //{
        //    Stream instream = null;
        //    StreamReader sr = null;
        //    HttpWebResponse response = null;
        //    HttpWebRequest request = null;
        //    Encoding encoding = Encoding.UTF8;
        //    // 准备请求...
        //    try
        //    {
        //        string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + secret;
        //        // 设置参数
        //        request = WebRequest.Create(url) as HttpWebRequest;
        //        CookieContainer cookieContainer = new CookieContainer();
        //        request.CookieContainer = cookieContainer;
        //        request.AllowAutoRedirect = true;
        //        request.Method = "GET";
        //        request.ContentType = "application/x-www-form-urlencoded";
        //        //发送请求并获取相应回应数据
        //        response = request.GetResponse() as HttpWebResponse;
        //        //直到request.GetResponse()程序才开始向目标网页发送Post请求
        //        instream = response.GetResponseStream();
        //        sr = new StreamReader(instream, encoding);
        //        //返回结果网页（html）代码
        //        string content = sr.ReadToEnd();
        //        string err = string.Empty;
        //        //Access_token token = new Access_token();
        //        //转为json对象
        //        JObject jo = (JObject)JsonConvert.DeserializeObject(content);
        //        string zone = jo["access_token"].ToString();
        //        string zone_en = jo["expires_in"].ToString();
        //        return zone;
        //    }

        //    catch (Exception ex)
        //    {
        //        string err = ex.Message;
        //        return string.Empty;
        //    }
        //}

        public string GetToken()
        {
            string appid = "wx13300bb589497266";
            string secret = "ef4764ea42b4ed48dbd809ee78a9744c";
            //string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + secret;
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}", appid, secret);

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            //GET请求
            request.Method = "GET";
            request.ReadWriteTimeout = 5000;
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream ResponseStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(ResponseStream, Encoding.GetEncoding("utf-8"));

            //返回内容
            string content = sr.ReadToEnd();
            JObject json = (JObject)JsonConvert.DeserializeObject(content);
            string access_token = json["access_token"].ToString();
            string expires_in = json["expires_in"].ToString();
            this.WriteToken(access_token);
            return access_token;
        }

        public void WriteToken(string token)
        {
            //【1】创建文件流
            FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(".")+"\\token.txt", FileMode.Create);
            //【2】创建写入器
            StreamWriter sw = new StreamWriter(fs, Encoding.Default);
            //【3】以流的方式写入数据
            sw.Write(token+","+DateTime.Now);
            //【4】关闭写入器
            sw.Close();
            //【5】关闭文件流
            fs.Close();
        }

        public string ReaderToken()
        {
            //【1】创建文件流
            FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(".") + "\\token.txt", FileMode.Open);
            //【2】创建读取器
            StreamReader sr = new StreamReader(fs,Encoding.Default);
            //【3】以流的方式读取数据
            string readToken = sr.ReadToEnd();
            //【4】关闭读取器
            sr.Close();
            //【5】关闭文件流
            fs.Close();
            return readToken;
        }
    }
}