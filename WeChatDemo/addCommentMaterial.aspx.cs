﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace WeChatDemo
{
    public partial class addCommentMaterial : System.Web.UI.Page
    {
        private string html = "<p style='font-size: 16px;white-space: normal;max-width: 100%;min-height: 1em;color: rgb(62, 62, 62);background-color: rgb(255, 255, 255);line-height: 25.6px;box-sizing: border-box !important;word-wrap: break-word !important;'><span style='max-width: 100%;line-height: 16px;font-family: 宋体;font-size: 14px;background-color: rgb(253, 234, 218);box-sizing: border-box !important;word-wrap: break-word !important;'><strong style='max-width: 100%;color: rgb(102, 102, 102);box-sizing: border-box !important;word-wrap: break-word !important;'><span style='padding: 4px 10px;max-width: 100%;color: rgb(0, 0, 0);box-shadow: rgb(165, 165, 165) 4px 4px 2px;box-sizing: border-box !important;word-wrap: break-word !important;'>导语</span></strong><br style='max-width: 100%;box-sizing: border-box !important;word-wrap: break-word !important;'></span></p><blockquote style='padding-top: 10px;padding-right: 10px;padding-bottom: 10px;border-top: 3px solid rgb(201, 201, 201);border-right: 3px solid rgb(201, 201, 201);border-bottom: 3px solid rgb(201, 201, 201);border-left-color: rgb(201, 201, 201);font-size: 16px;white-space: normal;max-width: 100%;color: rgb(62, 62, 62);background-color: rgb(255, 255, 255);line-height: 25.6px;box-shadow: rgb(170, 170, 170) 0px 0px 10px;box-sizing: border-box !important;word-wrap: break-word !important;'><p style='max-width: 100%;min-height: 1em;font-family: 微软雅黑;line-height: 25.6px;box-sizing: border-box !important;word-wrap: break-word !important;'><span style='background-color: rgb(253, 234, 218);color: rgb(38, 38, 38);font-family: 宋体;font-size: 14px;'>盛安德郑州团队近期在跟进一个新的澳洲机会，客户Matthew</span><span style='background-color: rgb(253, 234, 218);color: rgb(38, 38, 38);font-family: 宋体;font-size: 14px;'>来到中国，和团队针对项目启动前的需求确认和产品规划进行了更进一步的沟通。在近两周的时间里，客户和团队碰撞出了很多火花，同时也引发了团队的很多思考。从文中提到的几点开会过程中有意思的事情，我们能明确感受到：不管是客户还是团队，都在沟通的过程中不断修正自己的想法和思路，双方在齐心协力的为了同一个目标而努力，协同工作；客户和团队是合作伙伴，而不是简单的甲方乙方……</span></p></blockquote><br><p> Matthew是个澳洲客户，前期有过很长时间的沟通和推进，我们对业务和项目需求目标大概也了解。但是针对第一个要发布的版本，要做成具体什么样的产品还是两眼一抹黑。故此，客户来我们办公室两周，专门讨论具体细节。期望经过两周的密集讨论，我们能有若干产出，比如：</p><ul class='list-paddingleft-2' style='list-style-type: circle;'><li><p>所有的User Story</p></li><li><p>业务流程图清晰</p></li><li><p>数据库结构定义完毕</p></li><li><p>系统架构和特定功能点的技术方案</p></li><li><p>Wireframe定义完毕</p></li><li><p>核心页面的最终效果图</p></li><li><p>开发计划精确到每个Sprint的范围</p></li></ul><p style = 'text-align:center;box-sizing: border-box;margin-bottom: 15px;' ><img src= 'http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5SQA5c9xPY66sb0qYHCy34WT4VJE8OkhDaOBm1TVE7xdFJLxrDm8I7fRxpaG8PX1FAusL7SHwTibDjw/0?wx_fmt =jpeg'></ p ><br><p> 测试公众号.....</p><p> 测试文章.....</ p ><br><br><img src= 'http://mmbiz.qpic.cn/mmbiz_jpg/YzRib8XQo5SQA5c9xPY66sb0qYHCy34WTXJZPib5jyubJxyvgqkF67PNIl0A5V2eWtJ0U3RzZ9pub7iaZq3dRv6fQ/0?wx_fmt =jpeg' /> ";

        protected void Page_Load(object sender, EventArgs e)
        {
            //html = Server.UrlEncode(html);
            //html = Server.HtmlEncode(html);
            string token = new ACCESS_TOKEN().GetAccessToken();
            Response.Write(token);
            Response.Write("<br>");

            string jsonStr = "{{\"articles\":[{{\"title\":\"{0}\",\"thumb_media_id\":\"{1}\",\"author\":\"{2}\",\"digest\":\"{3}\",\"show_cover_pic\":{4},\"content\":\"{5}\",\"content_source_url\":\"{6}\",\"need_open_comment\":{7},\"only_fans_can_comment\":{8}}}]}}";
            jsonStr = string.Format(jsonStr, "与客户面对面确认需求是什么样的体验？", "K0FgA3ctFn92CDFw_ks93LFn_b87_IEXJRqVUCRwtmg", "Albert", "", 0, html, "http://www.shinetechchina.com.cn/services/softwareDevelopment.shtml#softDevelop",1,0);

            JObject obj = JObject.Parse(jsonStr);
            //var obj = new JavaScriptSerializer().Serialize(jsonStr);
            //obj = HttpContext.Current.Server.UrlDecode(obj);
            //obj = HttpContext.Current.Server.HtmlDecode(obj);
            // Response.Write(obj["articles"]);

            string url = string.Format("https://api.weixin.qq.com/cgi-bin/material/add_news?access_token={0}", token);
            HttpComment(url, obj.ToString());
        }

        public string HttpComment(string posturl, string postData)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            byte[] data = encoding.GetBytes(postData);
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(posturl) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                Response.Write(content);
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }
    }
}