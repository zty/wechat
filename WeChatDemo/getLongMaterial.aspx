﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="getLongMaterial.aspx.cs" Inherits="WeChatDemo.getLongMaterial1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <form id="Form1" runat="server">
     <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>获取永久素材 <small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!-- start form for validation -->
                        <div class="col-md-12 col-xs-12 ling">
                              <label for="email">MediaId * :</label>
                              <asp:TextBox ID="MediaId" runat="server" CssClass="form-control" placeholder="要获取的素材的media_id"></asp:TextBox>
                        </div>
                        <div class="col-md-12 col-xs-12 ling">
                               <label for="email">返回 * :</label>
                            <textarea id="ResultMaterial" runat="server" rows="17" class="form-control" placeholder=""></textarea>
                        </div>

                          <br/>
                        <div class="col-md-12">
                            <br/>
                            <asp:Button ID="BtnMaterial" runat="server" Text="提交" CssClass="btn btn-primary" OnClick="BtnMaterial_Click"  />
                        </div>
                    <!-- end form for validations -->

                  </div>
                </div>


              </div>
      </div>
           <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>获取素材列表 <small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start form for validation -->
                        <div class="col-md-4 col-xs-12 ling">
                              <label for="fullname">ID * :</label>
                              <select id="TypeName"  runat="server"   class="span12 form-control" required="required">
                                    <option value="news">图文</option>
                                    <option value="image">图片</option>
                                    <option value="video">视频</option>
                                    <option value="voice">语音</option>
                               </select>
                        </div>
             
                        <div class="col-md-4 col-xs-12 ling">
                              <label for="email">位置 * :</label>
                              <asp:TextBox ID="Offset" runat="server" CssClass="form-control" Text="0" placeholder="从全部素材的该偏移位置开始返回，0表示从第一个素材 返回"></asp:TextBox>
                        </div>
                        <div class="col-md-4 col-xs-12 ling">
                              <label for="fullname">数量 * :</label>
                              <asp:TextBox ID="Count" runat="server" CssClass="form-control" Text="20" placeholder="返回素材的数量，取值在1到20之间"></asp:TextBox>
                        </div>

                        <div class="col-md-12 col-xs-12 ling">
                               <label for="email">返回 * :</label>
                            <textarea id="ResultMaterialList" runat="server" rows="26" class="form-control" placeholder=""></textarea>
                        </div>

                          <br/>
                        <div class="col-md-12">
                            <br/>
                            <asp:Button ID="Button2" runat="server" Text="提交" CssClass="btn btn-primary" OnClick="Button2_Click" />
                        </div>
                       
                    <!-- end form for validations -->

                  </div>
                </div>


              </div>
      </div>
            </form>
    <script>
        function IsChecked(num) {
            switch (num) {
                case 0:
                    $('#ContentPlaceHolder1_ShowTure').prop('checked', true);
                    break;
                case 1:
                    $('#ContentPlaceHolder1_ShowFalse').prop('checked', true);
                    break;
                case 2:
                    $('#ContentPlaceHolder1_OpenTure').prop('checked', true);
                    break;
                case 3:
                    $('#ContentPlaceHolder1_OpenFalse').prop('checked', true);
                    break;
                case 4:
                    $('#ContentPlaceHolder1_FansTure').prop('checked', true);
                    break;
                case 5:
                    $('#ContentPlaceHolder1_FansFalse').prop('checked', true);
                    break;
            }
        }
    </script>
</asp:Content>
