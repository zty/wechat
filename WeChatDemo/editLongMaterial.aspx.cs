﻿using System;
using System.Collections.Generic;
using Models;
using DAL;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;

namespace WeChatDemo
{
    public partial class editLongMaterial1 : System.Web.UI.Page
    {
        private MaterialModel db = new MaterialModel();
        private HTTPRequest dal = new HTTPRequest();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex is HttpRequestValidationException)
            {
                Response.Write("<script language=javascript>alert('字符串含有非法字符！')</script>");
                Server.ClearError(); // 如果不ClearError()这个异常会继续传到Application_Error()
                Response.Write("<script language=javascript>window.location.href='default.aspx';</script>");
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            getData();
            string token = new ACCESS_TOKEN().GetAccessToken();
            string jsonStr = "{{\"media_id\":\"{0}\",\"index\":{1},\"articles\":{{\"title\":\"{2}\",\"thumb_media_id\":\"{3}\",\"author\":\"{4}\",\"digest\":\"{5}\",\"show_cover_pic\":{6},\"content\":\"{7}\",\"content_source_url\":\"{8}\",\"need_open_comment\":{9},\"only_fans_can_comment\":{10}}}}}";
            jsonStr = string.Format(jsonStr, db.MediaId, db.Index, db.Title, db.ThumbMediaId, db.Author, db.Digest, db.ShowCoverPic, db.Content, db.Url, db.NeedOpenComment, db.OnlyFansCanComment);
            JObject obj = JObject.Parse(jsonStr);
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/material/update_news?access_token={0}", token);
            string resJson = dal.HttpPost(url, obj.ToString());
            this.ResultTxt.Text = resJson;
        }
        public void getData()
        {
            db.MediaId = this.IndexID.Text;
            db.Index = this.Index.Text;
            db.Title = this.TitleTxt.Text;
            db.ThumbMediaId = this.MediaId.Text;
            db.Author = this.Author.Text;
            db.Digest = this.Digest.Text;
            db.Url = this.ContentSourceUrl.Text;
            db.Content = this.Content.InnerText;

            if (ShowTure.Checked)
                db.ShowCoverPic = Convert.ToInt32(this.ShowTure.Text);
            if (ShowFalse.Checked)
                db.ShowCoverPic = Convert.ToInt32(this.ShowFalse.Text);
            if (OpenTure.Checked)
                db.NeedOpenComment = Convert.ToInt32(this.OpenTure.Text);
            if (OpenFalse.Checked)
                db.NeedOpenComment = Convert.ToInt32(this.OpenFalse.Text);
            if (FansTure.Checked)
                db.OnlyFansCanComment = Convert.ToInt32(this.FansTure.Text);
            if (FansFalse.Checked)
                db.OnlyFansCanComment = Convert.ToInt32(this.FansFalse.Text);
        }
    }
}