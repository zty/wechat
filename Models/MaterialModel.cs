﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class MaterialModel
    {
        public string MediaId { get; set; }
        public string Index { get; set; }
        public string Title { get; set; }
        public string ThumbMediaId { get; set; }
        public string Author { get; set; }
        public string Url { get; set; }
        public string Content { get; set; }
        public string Digest { get; set; }
        public int ShowCoverPic { get; set; }
        public int NeedOpenComment { get; set; }
        public int OnlyFansCanComment { get; set; }
        public string Type { get; set; }
        public int Offset { get; set; }
        public int Count { get; set; }
    }
}
